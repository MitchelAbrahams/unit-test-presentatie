package Presentatie.mypicom;

import org.springframework.stereotype.Component;

@Component
public class CarTester {

    public void testCar(Car car) {
        car.startEngine();
        car.accelerate();
        car.brake();
        car.turnLeft();
        car.turnRight();
    }
}
