package Presentatie.mypicom;

import org.springframework.stereotype.Component;

@Component
public class CarSaleService {

    public double sellCar(Car car) {
        if (car instanceof Ferarri) {
            return (200000/4)*0.4;
        } else if (car instanceof Volkswagen) {
            return (50000/4)*0.4;
        }
        return 0;
    }
}
