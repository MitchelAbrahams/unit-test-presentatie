package Presentatie.mypicom;

public class Main {
    public static void main(String[] args) {
        Quicksort quicksort = new Quicksort();
        int[] speelkaarten = {3,6,5,4,8,7,2};

        System.out.println("speelkaarten voordat sort is gebeurt");
        for (int i = 0; i < speelkaarten.length; i++) {
            System.out.println(speelkaarten[i]);
        }

        int[] speelkaartenSorted = quicksort.quickSort(speelkaarten, 0, speelkaarten.length -1);

        System.out.println("speelkaarten gesorteerd");
        for (int i = 0; i < speelkaartenSorted.length; i++) {
            System.out.println(speelkaartenSorted[i]);
        }
    }
}
