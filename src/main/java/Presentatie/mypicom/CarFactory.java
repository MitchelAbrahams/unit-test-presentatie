package Presentatie.mypicom;

public class CarFactory {
    public Car createCar(int wheels,int doors, int horsePower, Car brand) {
        if (brand instanceof Ferarri) {
            return new Ferarri(wheels,doors,horsePower);
        } else if (brand instanceof Volkswagen) {
            return new Volkswagen(wheels,doors,horsePower);
        }
        return null;
    }
}
