package Presentatie.mypicom;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

// dit zou normaal een controller / api klasse zijn
public class CarGarageController {

    CarTester carTester;
    CarSaleService carSaleService;

    @Autowired
    public CarGarageController(CarTester carTester, CarSaleService carSaleService) {
        this.carTester = carTester;
        this.carSaleService = carSaleService;
    }

    public String testCar(Car car) {
        carTester.testCar(car);
        return "the car has been tested";
    }

    public double sellCar(Car car, boolean carSalesManLikesYou) {
        double refunds = carSaleService.sellCar(car);
        if (carSalesManLikesYou) {
            refunds /= 2;
        } else if (!carSalesManLikesYou) {
            refunds *=2;
        }
        return refunds;
    }
}
