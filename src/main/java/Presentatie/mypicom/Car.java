package Presentatie.mypicom;

public abstract class Car {
    int wheels;
    int doors;
    int horsePower;
    Boolean carStarted;

    public Car() {

    }

    public Car(int wheels, int doors, int horsePower) {
        this.wheels = wheels;
        this.doors = doors;
        this.horsePower = horsePower;
    }

    public void accelerate() {
        if (carStarted) {
            if (horsePower >= 50 && horsePower <= 120) {
                System.out.println("the car is not that fast, acceleration is slow");
            } else if (horsePower >= 120 && horsePower <= 300) {
                System.out.println("this is a pretty fast car!, acceleration is moderate");
            } else if (horsePower >= 300) {
                System.out.println("this car is really fast!!, acceleration is great");
            }
        } else {
            System.out.println("maybe you wanna start the engine first");
        }
    }

    public void brake() {
        System.out.println("the car is slowing down");
    }

    public void startEngine() {
        carStarted = true;
        System.out.println("the engine has been started");
    }

    public void turnLeft() {
        System.out.println("sharp turn left");
    }

    public void turnRight() {
        System.out.println("sharp trun right");
    }
}
